My personal mission is to improve user oriented performance to enable new work-flows. I do that by having an in-depth knowledge of how software and hardware work together.

* Co-owner EEVEE/Viewport module.
* Main Developer Vulkan Backend.
* Go-to person for any platform related issues.
* Contracted by Blender Institute since 2019.
* Contributed to Blender project since 2008.

Secondly I believe that Blender should aim at supporting as much hardware as possible. Most of us started out on crappy hardware and without the accessibility of tools like Blender would make us a different person then we are now. Although Blender officially supports hardware of the past 7 years, many older hardware can still be used.

In the past I worked at commercial companies on large innovative projects. I switched to open source development as I missed technologic challenges and working with similar minded folks. I fulfilled many roles including lead developer, team lead, project-architect and CTO.

In my spare time I hack on software music synthesizers. Most of the work is published at https://github.com/jeroenbakker-atmind

# Active Projects

* 2022-2024 Main developer of Blender Vulkan Backend.
* 2022-2024 Helping out as developer with EEVEE-Next. Parts that I have been working on are:
  * Cryptomatte render layer
  * Reflection probes
  * World probe
  * Co-developed with Clement the Planar probe.
  * Platform support & quality control

# BConf Presentations

* [GPU development]()
* [Vulkan Project Update](https://video.blender.org/w/bnrF73ZFiXpLMUXN2E5Diq)

# Past Projects

* 2010-2012 Tiled based compositor
* 2018 Workbench Viewport
* 2019 Cycles OpenCL improvements
* 2019 EEVEE Render Layers
* 2020 Asset Indexing
* 2021 3D texturing Brush
* 2021 Cryptomatte improvements